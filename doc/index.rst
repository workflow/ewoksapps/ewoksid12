ewoksid12
=========

*ewoksid12* provides data processing workflows for ID12.

*ewoksid12* has been developed by the `Software group <http://www.esrf.eu/Instrumentation/software>`_
and the ID12 staff of the `European Synchrotron <https://www.esrf.eu/>`_.

Documentation
-------------

.. toctree::
    :maxdepth: 2

    api
