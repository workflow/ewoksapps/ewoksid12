# CHANGELOG.md

## (unreleased)

## 0.1.1

Bug fixes:

- pyproject.toml requires error

## 0.1.0

New features:

- HDF5 to ASCII convertion specific to ID12
